
# Deploy an ECR Docker Repository Image to a Server (Simulated)
In this project, we will manually deploy a Docker Image that we have built and stored in an Amazon ECR. We'll use our local machine as a simulated server.

## Technologies Used
- Docker
- Amazon ECR
- Node.js
- MongoDB
- Mongo-Express
- Git
- Linux (Ubuntu)

## Project Description
- Copy Docker compose file to remote server
- Login to private Docker registry on remote server to fetch our app image
- Start our application container with MongoDB and Mongo-Express services using docker compose

## Prerequisites
- AWS Account
- A NodeJs application Docker Image uploaded to a Amazon ECR
- [The difference between Docker Compose and Docker-Compose](https://stackoverflow.com/questions/66514436/difference-between-docker-compose-and-docker-compose)

## Guide Steps
- From our local machine that will act as a server perform a `docker login` with your Amazon ECR. The command will look something like the one below.
	- `aws ecr get-login-password --region YOUR_LOCATION | docker login --username AWS --password-stdin YOUR_REPOSITORY_.amazonaws.com`
- We'll utilize our `mongo.yaml` file for our `Docker Compose` command. The file contains configurations for our app, MongoDB, and Mongo-Express to all talk to each other.
	- `docker compose -f mongo.yaml up`

![Docker Compose Success](/images/m7-4-docker-compose-successful.png)


We can see here our 3 containers are running

![Containers All Running](/images/m7-4-containers-running.png)

We can verify our application is running via the web browser

![Application Running](/images/m7-4-application-running.png)

When we make changes to our application we can see the changes are saved in our MongoDB container

![Application Change Made](/images/m7-4-application-change-made.png)

![MongoDB Storing Data Success](/images/m7-4-mongodb-stores-data.png)